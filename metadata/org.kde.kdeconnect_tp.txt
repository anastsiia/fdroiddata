Categories:System
License:GPL-2.0-only
Web Site:https://community.kde.org/KDE_Connect
Source Code:https://invent.kde.org/kde/kdeconnect-android
Issue Tracker:https://bugs.kde.org

Auto Name:KDE Connect
Summary:KDE Integration
Description:
KDE Connect provides a set of features to integrate your workflow across
devices:

* Shared clipboard: copy and paste between your devices.
* Share files and URLs to your computer from any app.
* Get notifications for incoming calls and SMS messages on your PC.
* Virtual touchpad: Use your phone screen as your computer's touchpad.
* Notifications sync: Read your Android notifications from the desktop.
* Multimedia remote control: Use your phone as a remote for Linux media players.
* WiFi connection: no USB wire or bluetooth needed.
* End-to-end TLS encryption: your information is safe.

Please note you will need to install KDE Connect on your computer for this app
to work, and keep the desktop version up-to-date with the Android version for
the latest features to work.

This app is part of an open source project and it exists thanks to all the
people who contributed to it. Visit the website to grab the source code.
.

Repo Type:git
Repo:https://anongit.kde.org/kdeconnect-android

Build:0.3.0,5
    commit=v0.3
    subdir=KdeConnect
    gradle=yes

Build:0.4,9
    commit=v0.4
    gradle=yes

Build:0.4.1,10
    commit=v0.4.1
    gradle=yes

Build:0.4.2,11
    commit=v0.4.2
    gradle=yes

Build:0.5,12
    commit=v0.5
    gradle=yes

Build:0.6,14
    commit=v0.6
    gradle=yes

Build:0.6.3,18
    commit=v0.6.3
    gradle=yes

Build:0.6.4,20
    commit=v0.6.4
    gradle=yes

Build:0.7,70
    commit=v0.7
    gradle=yes

Build:0.7.1,711
    commit=v0.7.1
    gradle=yes

Build:0.7.2,720
    commit=v0.7.2
    gradle=yes

Build:0.7.3,730
    commit=v0.7.3
    gradle=yes

Build:0.7.3.1,731
    commit=v0.7.3.1
    gradle=yes

Build:0.7.3.2,732
    commit=v0.7.3.2
    gradle=yes

Build:0.7.3.3,733
    commit=v0.7.3.3
    gradle=yes

Build:0.7.3.4,734
    disable=no tag
    commit=v0.7.3.4
    gradle=yes

Build:0.7.3.6,736
    commit=v0.7.3.6
    gradle=yes

Build:0.8,800
    commit=v0.8
    gradle=yes

Build:0.8d,804
    commit=v0.8d
    gradle=yes

Build:0.8e,805
    commit=v0.8e
    gradle=yes

Build:0.8g,807
    commit=v0.8g
    gradle=yes

Build:0.8h,808
    commit=v0.8h
    gradle=yes

Build:0.9.1,906
    commit=v0.9
    gradle=yes

Build:0.9c,907
    commit=v0.9b
    gradle=yes

Build:0.9d,908
    commit=v0.9d
    gradle=yes

Build:0.9e,909
    commit=v0.9e
    gradle=yes

Build:0.9g,910
    commit=v0.9g
    gradle=yes

Build:1.0,1000
    commit=v1.0
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.0.2,1020
    commit=v1.0.2
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.1,1102
    commit=v1.1
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.2,1200
    commit=v1.2
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.3.2,1320
    commit=v1.3
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.4,1400
    commit=v1.4
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.4.4,1440
    commit=v1.4.4
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.5,1500
    commit=v1.5
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.6,1600
    commit=v1.6
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.6.1,1610
    commit=v1.6.1
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.6.3,1630
    commit=v1.6.3
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.6.5,1650
    commit=v1.6.5
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.6.6,1660
    commit=v1.6.6
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.7.1,1710
    disable=Cannot resolve external dependency com.android.tools.build:gradle:3.0.1 because no repositories are defined
    commit=v1.7.1
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.7.2,1720
    commit=v1.7.2
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.8.0,1800
    commit=v1.8
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.8.1,1811
    commit=v1.8.1
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.8.2,1820
    commit=v1.8.2
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.8.4,1840
    commit=v1.8.4
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.8.4,1841
    commit=v1.8.4.1
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.10,11006
    commit=v1.10
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.10.1,11010
    commit=v1.10.1
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.11,11100
    commit=v1.11
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.12,11200
    commit=v1.12
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.12.5,11250
    commit=v1.12.5
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.12.6,11260
    commit=v1.12.6
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Build:1.12.7,11270
    commit=v1.12.7
    gradle=yes
    prebuild=sed -i -e '/javaMaxHeapSize/s/4g/2g/g' build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.12.7
Current Version Code:11270
